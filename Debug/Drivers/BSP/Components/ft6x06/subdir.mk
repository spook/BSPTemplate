################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/BSP/Components/ft6x06/ft6x06.c 

OBJS += \
./Drivers/BSP/Components/ft6x06/ft6x06.o 

C_DEPS += \
./Drivers/BSP/Components/ft6x06/ft6x06.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/Components/ft6x06/%.o: ../Drivers/BSP/Components/ft6x06/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32 -DSTM32F4 -DSTM32F469NIHx -DSTM32F469I_DISCO -DDEBUG -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/inc" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/HAL/Inc" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/CMSIS/Include" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/BSP/STM32469I-Discovery" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



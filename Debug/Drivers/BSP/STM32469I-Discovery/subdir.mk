################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_audio.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_eeprom.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_lcd.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_qspi.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_sd.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_sdram.c \
../Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_ts.c 

OBJS += \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_audio.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_eeprom.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_lcd.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_qspi.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_sd.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_sdram.o \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_ts.o 

C_DEPS += \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_audio.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_eeprom.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_lcd.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_qspi.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_sd.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_sdram.d \
./Drivers/BSP/STM32469I-Discovery/stm32469i_discovery_ts.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/STM32469I-Discovery/%.o: ../Drivers/BSP/STM32469I-Discovery/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32 -DSTM32F4 -DSTM32F469NIHx -DSTM32F469I_DISCO -DDEBUG -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/inc" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/HAL/Inc" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/CMSIS/Include" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/BSP/STM32469I-Discovery" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



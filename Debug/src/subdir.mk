################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/main.c \
../src/stm32f4xx_hal_msp.c \
../src/stm32f4xx_it.c \
../src/system_stm32f4xx.c 

OBJS += \
./src/main.o \
./src/stm32f4xx_hal_msp.o \
./src/stm32f4xx_it.o \
./src/system_stm32f4xx.o 

C_DEPS += \
./src/main.d \
./src/stm32f4xx_hal_msp.d \
./src/stm32f4xx_it.d \
./src/system_stm32f4xx.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32 -DSTM32F4 -DSTM32F469NIHx -DSTM32F469I_DISCO -DDEBUG -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/inc" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/HAL/Inc" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/CMSIS/Include" -I"D:/Dokumenty/Dev/Eclipse/BSPTemplate/Drivers/BSP/STM32469I-Discovery" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


